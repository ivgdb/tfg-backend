<?php

namespace App\Tests\Functional;

use App\Tests\UserController\ActivateUserTest;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class BaseTest extends WebTestCase
{
    public static $token;
    public static $client;

    public function logout()
    {
        static::$token = null;
    }

    public function login($email, $password = '1234')
    {
        static::$client = static::createClient();
        static::$client->request('Post', '/api/login', [
            'username' => $email,
            'password' => $password
        ]);
        $response = static::$client->getResponse();

        if ($response->getStatusCode() === 200) {
            static::$token = json_decode(static::$client->getResponse()->getContent(), true)['token'];
        } else {
            throw new \Exception('BAD CREDENTIALS: try doctrine:fixtures:load');
        }

        return $this->getCurrentUser();
    }


    private function getCurrentUser()
    {
        return $this->get('/api/users/current');
    }

    public function get($path, $query = array(), $checkError = true)
    {
        static::$client = static::createClient();
        $token = static::$token;
        static::$client->request('Get', $path, $query, array(), array('HTTP_AUTHORIZATION' => "Bearer {$token}"));

        $response = json_decode(static::$client->getResponse()->getContent(), true);
        if ($checkError) {
            $this->checkNotHasError($response);
        }

        return $response;
    }

    public function getError($path, $query = array())
    {
        return $this->get($path, $query, false);
    }

    public function post($path, $body = array(), $checkError = true)
    {
        static::$client = static::createClient();
        $token = static::$token;
        static::$client->request('Post', $path, $body, array(), array('HTTP_AUTHORIZATION' => "Bearer {$token}"));

        $response = json_decode(static::$client->getResponse()->getContent(), true);

        if ($checkError) {
            $this->checkNotHasError($response);
        }

        return $response;
    }

    public function postError($path, $body = array())
    {
        return $this->post($path, $body, false);
    }

    public function delete($path, $checkError = true)
    {
        static::$client = static::createClient();
        $token = static::$token;
        static::$client->request('Delete', $path, array(), array(), array('HTTP_AUTHORIZATION' => "Bearer {$token}"));

        $response = json_decode(static::$client->getResponse()->getContent(), true);

        if ($checkError) {
            $this->checkNotHasError($response);
        }

        return $response;
    }

    public function deleteError($path)
    {
        return $this->delete($path, false);
    }

    public function checkNotHasError($request)
    {
        if (isset($request['message'])) {
            $this->assertArrayNotHasKey('message', $request, $request['message']);
        }
    }
}
