<?php

namespace App\Tests\Functional;

use PHPUnit\Framework\TestCase;

class GetCurrentUserTest extends BaseTest
{
    public function testGetCurrentUser()
    {
        $this->login('robot@q.com');
        $result = $this->get('/api/users/current');
        var_dump($result);
        self::assertTrue($result['success']);
    }
}
