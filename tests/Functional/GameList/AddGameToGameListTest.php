<?php

namespace App\Tests\Functional\GameList;

use App\Tests\Functional\BaseTest;
use PHPUnit\Framework\TestCase;

class AddGameToGameListTest extends BaseTest
{
    public function testAddGameToGameList()
    {
        $user = $this->login('robot@q.com', '1234');
        $videogames = $this->get('/api/games/');
        $gameList = $this->post('/api/gamelists/'.$user['gameLists'][0]['id'].'/add', [
            'videogame' => $videogames[0]['uuid']
        ]);
        self::assertContains($videogames[0], $gameList['videogames']);
    }
}
