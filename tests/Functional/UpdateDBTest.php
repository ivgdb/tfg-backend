<?php

namespace App\Tests\Functional;


class UpdateDBTest extends BaseTest
{
    public function testUpdateDB()
    {
        $this->login('robot@q.com');
        $result = $this->post('/api/games/updateDB');
        self::assertTrue($result['success']);
    }
}
