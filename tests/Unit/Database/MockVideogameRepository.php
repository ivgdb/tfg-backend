<?php


namespace App\Tests\Unit\Database;


use App\Repository\VideogameRepository;

class MockVideogameRepository extends VideogameRepository
{
    private $game;

    public function __construct($game = null)
    {
        $this->game = $game;
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->game;
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->game;
    }

    public function findAll()
    {
        return $this->game;
    }
}