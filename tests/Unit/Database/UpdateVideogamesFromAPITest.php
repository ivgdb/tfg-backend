<?php

namespace App\Tests\Unit\Database;

use App\UseCases\Database\UpdateDatabaseFromApiUseCase;
use PHPUnit\Framework\TestCase;

class UpdateVideogamesFromAPITest extends TestCase
{
    public function testUpdateDatabase()
    {
        $queries = ['batman'];
        $videogames = (new UpdateDatabaseFromApiUseCase(new MockVideogameRepository()))->execute('https://api-v3.igdb.com/games', $queries);
        var_dump($videogames);
        self::assertNotEmpty($videogames);
    }
}
