<?php


namespace App\Tests\Unit\User;


use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class MockPasswordEncoder implements UserPasswordEncoderInterface
{
    public function encodePassword(UserInterface $user, $plainPassword)
    {
        return $plainPassword;
    }

    public function isPasswordValid(UserInterface $user, $raw)
    {
    }
}