<?php


namespace App\Tests\Unit\User;


use App\Repository\UserRepository;

class MockUserRepository extends UserRepository
{
    private $user;

    public function __construct($user)
    {
        if($user){
            $this->user = $user;
        }
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->user;
    }

    public function findAll()
    {
        return $this->user;
    }
}