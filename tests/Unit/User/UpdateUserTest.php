<?php

namespace App\Tests\Unit\User;

use App\Entity\User;
use App\Enum\Errors;
use App\UseCases\User\UpdateUserUseCase;
use PHPUnit\Framework\TestCase;

class UpdateUserTest extends TestCase
{
    private $user;

    protected function setUp(): void
    {
        $this->user = new User('a@a.a', 'MrRobot', 'Elliot');
    }

    public function testUpdatedUser()
    {
        (new UpdateUserUseCase(new MockPasswordEncoder()))->execute($this->user, '   penA@gmAil.com    ','  Pena    ', '912345678', 'Veni Vidi Vici');


        self::assertEquals('pena@gmail.com', $this->user->getUsername());
        self::assertEquals('912345678', $this->user->getPassword());
        self::assertEquals('Veni Vidi Vici', $this->user->getName());
        self::assertEquals('Pena', $this->user->getNickname());
    }
}
