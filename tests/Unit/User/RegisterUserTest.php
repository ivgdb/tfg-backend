<?php

namespace App\Tests\Unit\User;

use App\Enum\Errors;
use App\UseCases\User\RegisterUserUseCase;
use PHPUnit\Framework\TestCase;

class RegisterUserTest extends TestCase
{
    public function testUserShouldHaveEmail()
    {
        $this->expectExceptionMessage(Errors::USER_SHOULD_HAVE_EMAIL);
        (new RegisterUserUseCase(new MockUserRepository(false), new MockPasswordEncoder()))->execute(null);
    }

    public function testUserShouldNotExist()
    {
        $this->expectExceptionMessage(Errors::USER_ALREADY_EXISTS);
        (new RegisterUserUseCase(new MockUserRepository(true), new MockPasswordEncoder()))->execute('a@a.a');
    }

    public function testUserShouldHavePassword()
    {
        $this->expectExceptionMessage(Errors::USER_SHOULD_HAVE_PASSWORD);
        (new RegisterUserUseCase(new MockUserRepository(false), new MockPasswordEncoder()))->execute('a@a.a');
    }

    public function testUserShouldHaveNickname()
    {
        $this->expectExceptionMessage(Errors::USER_SHOULD_HAVE_NICKNAME);
        (new RegisterUserUseCase(new MockUserRepository(false), new MockPasswordEncoder()))->execute('a@a.a', '12345');
    }

    public function testUserRegistered()
    {
        $user = (new RegisterUserUseCase(new MockUserRepository(false), new MockPasswordEncoder()))->execute('a@a.a', '1234', '1234', 'MrRobot', 'Elliot Alderson');
       var_dump($user);
        self::assertEquals('a@a.a', $user->getUsername());
        self::assertEquals('1234', $user->getPassword());
        self::assertEquals('MrRobot', $user->getNickname());
        self::assertEquals('Elliot Alderson', $user->getName());
    }
}
