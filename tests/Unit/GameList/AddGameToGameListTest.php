<?php

namespace App\Tests\Unit\GameList;


use App\Entity\GameList;
use App\Entity\User;
use App\Entity\Videogame;
use App\Enum\Errors;
use App\Repository\GameListRepository;
use App\UseCases\GameList\AddGameToGameListUseCase;
use PHPUnit\Framework\TestCase;

class AddGameToGameListTest extends TestCase
{
    private $gameList;
    private $videogame;
    private $user;

    public function setUp(): void
    {
        $this->videogame = new Videogame(
          'GodOfWar',
          'GodOfWarGodOfWarGodOfWar',
          new \DateTime()
        );
        $this->user = new User(
            'a@aaa.com',
            'aa',
            'easda'
        );
        $this->gameList = new GameList(
            $this->user,
            'listAAA'
        );
        $this->gameList->addGame($this->videogame);
    }

    public function testUserDoesNotOwnGameList()
    {
        $user = new User(
            'b@bbb.com',
            'bb',
            'mmm'
        );

        $this->expectExceptionMessage(Errors::FORBIDDEN);
        (new AddGameToGameListUseCase(new MockGameListRepository($this->gameList)))->execute($user, $this->gameList, $this->videogame);
    }

    public function testVideogameAlreadyAdded()
    {
        $this->expectExceptionMessage(Errors::GAME_ALREADY_ADDED);
        (new AddGameToGameListUseCase(new MockGameListRepository($this->gameList)))->execute($this->user, $this->gameList, $this->videogame);
    }

    public function testAddGameToGameList()
    {
        $gameList = (new AddGameToGameListUseCase(new MockGameListRepository(null)))->execute($this->user, $this->gameList, $this->videogame);
        self::assertEquals($this->gameList->getVideogames(), $gameList->getVideogames());
    }
}
