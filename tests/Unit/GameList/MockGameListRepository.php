<?php


namespace App\Tests\Unit\GameList;


use App\Entity\GameList;
use App\Entity\Videogame;
use App\Repository\GameListRepository;

class MockGameListRepository extends GameListRepository
{
    private $gameList;

    public function __construct($gameList = null)
    {
        $this->gameList = $gameList;
    }

    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->gameList;
    }

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->gameList;
    }

    public function findWithGame(Videogame $videogame, GameList $gameList)
    {
        return $this->gameList;
    }
}