<?php

namespace App\Repository;

use App\Entity\Ratings;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ratings|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ratings|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ratings[]    findAll()
 * @method Ratings[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RatingsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ratings::class);
    }

    public function orderUserRatingsByDate(User $user)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('r.createdDate', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function orderUserRatingsByFranchise(User $user)
    {
        return $this->createQueryBuilder('r')
            ->leftJoin('r.videogame', 'v')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('v.franchise', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function orderUserRatingsByGame(User $user)
    {
        return $this->createQueryBuilder('r')
            ->leftJoin('r.videogame', 'v')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('v.title', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function orderUserRatingsByRating(User $user)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('r.rating', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return Ratings[] Returns an array of Ratings objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ratings
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
