<?php

namespace App\Repository;

use App\Entity\GameList;
use App\Entity\Videogame;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GameList|null find($id, $lockMode = null, $lockVersion = null)
 * @method GameList|null findOneBy(array $criteria, array $orderBy = null)
 * @method GameList[]    findAll()
 * @method GameList[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameListRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GameList::class);
    }

    // /**
    //  * @return GameList[] Returns an array of GameList objects
    //  */

//    public function findWithGame($videogameUuid, $gameListUuid)
//    {
//        return $this->createQueryBuilder('g')
//            ->leftJoin('g.videogames', 'v')
//            ->where('v.uuid = :videogameUuid')
//            ->andWhere('g.uuid = :gameListUuid')
//            ->setParameter('videogameUuid', $videogameUuid)
//            ->setParameter('gameListUuid', $gameListUuid)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

    public function findWithGame($videogame,$gameList)
    {
        return $this->createQueryBuilder('g')
            ->leftJoin('g.videogames', 'v')
            ->where('v.uuid = :videogame')
            ->andWhere('g.uuid = :gameList')
            ->setParameter('videogame', $videogame)
            ->setParameter('gameList', $gameList->getUuid())
            ->getQuery()
            ->getResult()
        ;
    }

    public function persist(GameList $gameList)
    {
        $this->getEntityManager()->persist($gameList);
    }

    /*
    public function findOneBySomeField($value): ?GameList
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
