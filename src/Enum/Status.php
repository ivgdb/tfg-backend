<?php


namespace App\Enum;


class Status
{
    const ACTIVE = 'TICKET_ACTIVE';
    const DISABLED = 'TICKET_DISABLED';
    const EXPIRED = 'TICKET_EXPIRED';
    const OVERWRITTEN = 'TICKET_OVERWRITTEN';

}
