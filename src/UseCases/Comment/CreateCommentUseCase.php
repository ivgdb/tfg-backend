<?php


namespace App\UseCases\Comment;


use App\Entity\Comment;
use App\Entity\User;
use App\Enum\Errors;
use App\Repository\CommentRepository;
use App\Repository\VideogameRepository;

class CreateCommentUseCase
{
    /**
     * @var VideogameRepository
     */
    private $videogameRepository;

    public function __construct(VideogameRepository $videogameRepository)
    {
        $this->videogameRepository = $videogameRepository;
    }

    public function execute(User $user, $videogameUuid, $comment)
    {
        $videogame = $this->videogameRepository->findOneBy(['uuid' => $videogameUuid]);
        if(!$videogame){
            Errors::throw(Errors::VIDEOGAME_NOT_FOUND);
        }
        if(!$comment) {
            Errors::throw(Errors::EMPTY_COMMENT);
        }

        $comment = new Comment($user, $videogame, $comment);
        $user->addComment($comment);

        return $comment;
    }
}