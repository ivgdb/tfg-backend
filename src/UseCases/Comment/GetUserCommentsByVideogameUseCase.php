<?php


namespace App\UseCases\Comment;


use App\Entity\User;
use App\Enum\Errors;
use App\Repository\CommentRepository;
use App\Repository\VideogameRepository;

class GetUserCommentsByVideogameUseCase
{
    /**
     * @var VideogameRepository
     */
    private $videogameRepository;
    /**
     * @var CommentRepository
     */
    private $commentRepository;

    public function __construct(VideogameRepository $videogameRepository, CommentRepository $commentRepository)
    {
        $this->videogameRepository = $videogameRepository;
        $this->commentRepository = $commentRepository;
    }

    public function execute(User $user, $videogameUuid)
    {
        $videogame = $this->videogameRepository->findOneBy(['uuid' => $videogameUuid]);
        if(!$videogame){
            Errors::throw(Errors::VIDEOGAME_NOT_FOUND);
        }

        $comments = $this->commentRepository->findBy([
            'user' => $user,
            'videogame' => $videogame
        ]);

        return $comments;
    }
}