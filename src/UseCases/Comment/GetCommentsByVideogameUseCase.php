<?php


namespace App\UseCases\Comment;


use App\Entity\Videogame;
use App\Enum\Errors;
use App\Repository\CommentRepository;
use App\Repository\VideogameRepository;

class GetCommentsByVideogameUseCase
{
    /**
     * @var VideogameRepository
     */
    private $videogameRepository;
    /**
     * @var CommentRepository
     */
    private $commentRepository;

    public function __construct(VideogameRepository $videogameRepository, CommentRepository $commentRepository)
    {
        $this->videogameRepository = $videogameRepository;
        $this->commentRepository = $commentRepository;
    }

    public function execute(Videogame $videogame)
    {
        $comments = $this->commentRepository->orderVideogameCommentsByDate($videogame);
        return $comments;
    }
}