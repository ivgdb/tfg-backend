<?php


namespace App\UseCases\Comment;


use App\Entity\User;
use App\Repository\CommentRepository;

class GetCommentsByUserUseCase
{
    /**
     * @var CommentRepository
     */
    private $commentRepository;

    public function __construct(CommentRepository $commentRepository)
    {
        $this->commentRepository = $commentRepository;
    }

    public function execute(User $user)
    {
        $comments = $this->commentRepository->orderUserCommentsByDate($user);
        return $comments;
    }
}