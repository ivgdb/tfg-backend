<?php


namespace App\UseCases\Database;


use App\Entity\Videogame;
use App\Enum\Errors;
use App\Repository\VideogameRepository;
use Doctrine\ORM\EntityManagerInterface;
use Unirest;

class UpdateDatabaseFromApiUseCase
{
    private $videoGameRepository;
    private $request;
    public function __construct(VideogameRepository $videogameRepository)
    {
        $this->videoGameRepository = $videogameRepository;
    }

    //TODO: REFACTORIZAR
    public function execute($request, $query)
    {
        Unirest\Request::verifyPeer(false);
        $headers = array('Accept' => 'application/json', 'user-key' => '71159389324c8657eae1833c84074b45');
        $body = array('search' => $query,'fields' => 'age_ratings,aggregated_rating,aggregated_rating_count,alternative_names,artworks,bundles,category,collection,cover,created_at,dlcs,expansions,external_games,first_release_date,follows,franchise,franchises,game_engines,game_modes,genres,hypes,involved_companies,keywords,multiplayer_modes,name,parent_game,platforms,player_perspectives,popularity,pulse_count,rating,rating_count,release_dates,screenshots,similar_games,slug,standalone_expansions,status,storyline,summary,tags,themes,time_to_beat,total_rating,total_rating_count,updated_at,url,version_parent,version_title,videos,websites');
        $this->request =(array) json_decode(Unirest\Request::get($request, $headers, $body)->raw_body, true);

        $videogames = [];
        foreach ($this->request as $game)
        {
            if(array_key_exists('rating', $game) && array_key_exists('rating_count', $game) && array_key_exists('summary', $game) && array_key_exists('name', $game) && array_key_exists('first_release_date', $game))
            {
                if(!is_null($game['summary']) && !is_null($game['name']) && !is_null($game['first_release_date']))
                {
                    if(strlen($game['summary'])>5000){
                        continue;
                    }
                    var_dump($game);
                    $videogameExists = $this->videoGameRepository->findOneBy(['title' => $game['name']]);
                    if($videogameExists){
                        continue;
                    }
                    $videogame = new Videogame(
                        $game['name'],
                        $game['summary'],
                        new \DateTimeImmutable(date('Y-m-d\TH:i:sO', $game['first_release_date'] ))
                    );

                    if($game['rating'] && $game['rating_count'])
                    {
                        $videogame->setRating($game['rating']);
                        $videogame->setRatingCount($game['rating_count']);
                    }

                    if(array_key_exists('aggregated_rating', $game))
                    {
                        $videogame->setMetacritic($game['aggregated_rating']);
                    }

                    if(array_key_exists('storyline', $game)){
                        if(strlen($game['storyline'])<=5000){
                            $videogame->setStoryline($game['storyline']);
                        }
                    }

                    if(array_key_exists('cover', $game)){
                        $body = array(
                            'fields' => 'image_id',
                            'filter' => [
                                'id' => [
                                    'eq' => (string)$game['cover']
                                ]
                            ]
                        );

                        $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/covers', $headers, $body)->raw_body, true);
                        $url = 'https://images.igdb.com/igdb/image/upload/t_original/'.$request[0]['image_id'].'.jpg';
                        $videogame->setCover($url);
                    }

                    if(array_key_exists('platforms', $game))
                    {
                        $platforms = [];
                        foreach ($game['platforms'] as $platform){
                            $body = array(
                                'fields' => 'name',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$platform
                                    ]
                                ]
                            );

                            $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/platforms', $headers, $body)->raw_body, true);
                            $platforms[] = $request[0]['name'];
                        }

                        $videogame->setPlatforms($platforms);
                    }

                    if(array_key_exists('franchise', $game)){
                        $body = array(
                            'fields' => 'name',
                            'filter' => [
                                'id' => [
                                    'eq' => (string)$game['franchise']
                                ]
                            ]
                        );

                        $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/franchises', $headers, $body)->raw_body, true);
                        $franchise = $request[0]['name'];

                        $videogame->setFranchise($franchise);
                    }

                    if(array_key_exists('genres', $game)) {


                        $genres = [];

                        foreach ($game['genres'] as $genre){
                            $body = array(
                                'fields' => 'name',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$genre
                                    ]
                                ]
                            );

                            $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/genres', $headers, $body)->raw_body, true);
                            $genres[] = $request[0]['name'];
                        }

                        $videogame->setGenres($genres);
                    }

                    if(array_key_exists('involved_companies', $game)){
                        $developers = [];
                        $publishers = [];

                        foreach ($game['involved_companies'] as $involved_company){
                            $body = array(
                                'fields' => 'developer,publisher',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$involved_company
                                    ]
                                ]
                            );

                            $mainRequest =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/involved_companies', $headers, $body)->raw_body, true);
                            if($mainRequest[0]['developer']){
                                $body = array(
                                    'fields' => 'name',
                                    'filter' => [
                                        'id' => [
                                            'eq' => (string)$mainRequest[0]['id']
                                        ]
                                    ]
                                );

                                $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/companies', $headers, $body)->raw_body, true);
                                if(count($request)>0){
                                    $developers[] = $request[0]['name'];
                                }
                            }

                            if($mainRequest[0]['publisher']){
                                $body = array(
                                    'fields' => '*',
                                    'filter' => [
                                        'id' => [
                                            'eq' => (string)$mainRequest[0]['id']
                                        ]
                                    ]
                                );

                                $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/companies', $headers, $body)->raw_body, true);
                                if(count($request)>0){
                                    //TODO: SOME REQUESTS RETURN EMPTY ARRAY
                                    $publishers[] = $request[0]['name'];
                                }
                            }
                        }
                        $videogame->setDevelopers($developers);
                        $videogame->setPublishers($publishers);
                    }

                    if(array_key_exists('screenshots', $game)){
                        $screenshots = [];
                        foreach ($game['screenshots'] as $screenshot){
                            $body = array(
                                'fields' => 'image_id',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$screenshot
                                    ]
                                ]
                            );

                            $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/screenshots', $headers, $body)->raw_body, true);
                            $url = 'https://images.igdb.com/igdb/image/upload/t_original/'.$request[0]['image_id'].'.jpg';
                            $screenshots[] = $url;
                        }

                        $videogame->setScreenshots($screenshots);
                    }

                    if(array_key_exists('artworks', $game)){
                        $artworks = [];
                        var_dump($game['artworks']);
                        foreach ($game['artworks'] as $artwork){
                            $body = array(
                                'fields' => 'image_id',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$artwork
                                    ]
                                ]
                            );

                            $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/artworks', $headers, $body)->raw_body, true);
                            $url = 'https://images.igdb.com/igdb/image/upload/t_original/'.$request[0]['image_id'].'.jpg';
                            $artworks[] = $url;
                        }
                        $videogame->setArtworks($artworks);
                    }

                    if(array_key_exists('videos', $game)){
                        $videos = [];

                        foreach ($game['videos'] as $video){
                            $body = array(
                                'fields' => 'video_id',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$video
                                    ]
                                ]
                            );

                            $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/game_videos', $headers, $body)->raw_body, true);
                            $videos[] = 'https://www.youtube.com/embed/'.$request[0]['video_id'];
                        }
                        $videogame->setVideos($videos);
                    }

                    if(array_key_exists('websites', $game)){
                        $websites = [];

                        foreach ($game['websites'] as $website){
                            $body = array(
                                'fields' => 'category, url',
                                'filter' => [
                                    'id' => [
                                        'eq' => (string)$website
                                    ]
                                ]
                            );

                            $request =(array) json_decode(Unirest\Request::get('https://api-v3.igdb.com/websites', $headers, $body)->raw_body, true);
                            $websites[] = $request[0];
                            var_dump($request);
                        }
                        $videogame->setWebsites($websites);
                    }
                    var_dump($websites);
                    $videogames[] = $videogame;
                }
            }

        }

        return $videogames;
    }
}