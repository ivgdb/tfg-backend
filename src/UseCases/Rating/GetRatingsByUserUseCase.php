<?php


namespace App\UseCases\Rating;


use App\Entity\User;
use App\Repository\RatingsRepository;

class GetRatingsByUserUseCase
{
    /**
     * @var RatingsRepository
     */
    private $ratingsRepository;

    public function __construct(RatingsRepository $ratingsRepository)
    {
        $this->ratingsRepository = $ratingsRepository;
    }

    public function execute(User $user, $order)
    {
        switch ($order){
            case 'date':
                return $this->ratingsRepository->orderUserRatingsByDate($user);
                break;
            case 'franchise':
                return $this->ratingsRepository->orderUserRatingsByFranchise($user);
                break;
            case 'game':
                return $this->ratingsRepository->orderUserRatingsByGame($user);
                break;
            case 'rating':
                return $this->ratingsRepository->orderUserRatingsByRating($user);
                break;
            default:
                return $user->getRatings();
        }

    }
}