<?php


namespace App\UseCases\Rating;


use App\Entity\User;
use App\Entity\Videogame;
use App\Repository\RatingsRepository;

class RemoveRatingFromGameUseCase
{
    private $ratingsRepository;

    public function __construct(RatingsRepository $ratingsRepository)
    {
        $this->ratingsRepository = $ratingsRepository;
    }

    public function execute(User $user, Videogame $videogame)
    {
        $rating = $this->ratingsRepository->findOneBy([
            'user' => $user,
            'videogame' => $videogame
        ]);

        $videogame->removeRatings($rating);
    }
}