<?php


namespace App\UseCases\Rating;


use App\Entity\Ratings;
use App\Entity\User;
use App\Enum\Errors;
use App\Repository\RatingsRepository;
use App\Repository\VideogameRepository;

class AddRatingToGameUseCase
{
    private $ratingsRepository;
    private $videogameRepository;

    public function __construct(RatingsRepository $ratingsRepository, VideogameRepository $videogameRepository)
    {
        $this->ratingsRepository = $ratingsRepository;
        $this->videogameRepository = $videogameRepository;
    }

    public function execute(User $user, $videogameUuid, $rating)
    {
        if(!$videogameUuid)
        {
            Errors::throw(Errors::RATING_SHOULD_HAVE_VIDEOGAME);
        }

        if(!$rating)
        {
            Errors::throw(Errors::RATING_SHOULD_HAVE_RATING);
        }


        $videogame = $this->videogameRepository->findOneBy(['uuid' => $videogameUuid]);

        $ratingExists = $this->ratingsRepository->findOneBy(['user' => $user, 'videogame' => $videogame]);

        if($ratingExists) {
            $ratingExists->setRating($rating);
            return $ratingExists;
        }

        return new Ratings($user, $videogame, $rating);

    }
}