<?php


namespace App\UseCases\Rating;


use App\Entity\User;
use App\Entity\Videogame;
use App\Enum\Errors;
use App\Repository\RatingsRepository;
use App\Repository\VideogameRepository;

class GetByUserAndVideogameUseCase
{
    /**
     * @var RatingsRepository
     */
    private $ratingsRepository;
    /**
     * @var VideogameRepository
     */
    private $videogameRepository;

    public function __construct(RatingsRepository $ratingsRepository, VideogameRepository $videogameRepository)
    {
        $this->ratingsRepository = $ratingsRepository;
        $this->videogameRepository = $videogameRepository;
    }

    public function execute(User $user, $videogameUuid)
    {
       if(!$videogameUuid){
           Errors::throw(Errors::REQUEST_SHOULD_HAVE_VIDEOGAME_UUID);
       }

       $videogame = $this->videogameRepository->findOneBy(['uuid' => $videogameUuid]);
       if(!$videogame) {
           Errors::throw(Errors::VIDEOGAME_NOT_FOUND);
       }

       $rating = $this->ratingsRepository->findOneBy([
            'user' => $user,
            'videogame' => $videogame
       ]);

        return $rating;
    }
}