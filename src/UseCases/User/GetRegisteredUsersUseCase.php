<?php


namespace App\UseCases\User;


use App\Repository\UserRepository;

class GetRegisteredUsersUseCase
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute()
    {
        return $this->userRepository->findAll();
    }
}