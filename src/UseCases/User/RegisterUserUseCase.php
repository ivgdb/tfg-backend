<?php


namespace App\UseCases\User;


use App\Entity\GameList;
use App\Entity\User;
use App\Enum\Errors;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterUserUseCase
{
    private $userRepository;
    private $encoder;

    public function __construct(UserRepository $userRepository, ?UserPasswordEncoderInterface $encoder)
    {
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    public function execute(?string $email, ?string $password = null, ?string $passwordRepeat = null, ?string $nickname = null, ?string $name = null)
    {
        if(!$email)
        {
            Errors::throw(Errors::USER_SHOULD_HAVE_EMAIL);
        }

        $userExists = $this->userRepository->findOneBy(['email' => $email]);
        if($userExists)
        {
            Errors::throw(Errors::USER_ALREADY_EXISTS);
        }

        if(!$nickname)
        {
            Errors::throw(Errors::USER_SHOULD_HAVE_NICKNAME);
        }

        $userExists = $this->userRepository->findOneBy(['nickname' => $nickname]);
        if($userExists)
        {
            Errors::throw(Errors::NICKNAME_ALREADY_EXISTS);
        }

        if(!$password)
        {
            Errors::throw(Errors::USER_SHOULD_HAVE_PASSWORD);
        }

        if(!$passwordRepeat)
        {
            Errors::throw(Errors::USER_SHOULD_HAVE_REPEATED_PASSWORD);
        }

        if($password !== $passwordRepeat)
        {
            Errors::throw(Errors::PASSWORDS_DO_NOT_MATCH);
        }

        $user = new User($email, $nickname, $name);

        $user->setPassword($this->encoder->encodePassword($user, $password));
        $favoriteGamesList = new GameList($user, 'Favorite Games');
        $wishList = new GameList($user, 'Wish List');
        $user->addGameList($favoriteGamesList);
        $user->addGameList($wishList);

        $this->userRepository->persist($user);

        return $user;
    }

}