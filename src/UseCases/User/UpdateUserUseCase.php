<?php


namespace App\UseCases\User;


use App\Entity\User;
use App\Enum\Errors;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Normalizer\DataUriNormalizer;


class UpdateUserUseCase
{
    private $encoder;

    public function __construct( ?UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function execute(User $user, ?string $email, ?string $nickname, ?string $password, ?string $passwordRepeat, ?string $name, $avatar)
    {
        $user->setUsername($email);
        $user->setName($name);
        $user->setNickname($nickname);

        if($password)
        {
            if(!$passwordRepeat)
            {
                Errors::throw(Errors::USER_SHOULD_HAVE_REPEATED_PASSWORD);
            }

            if($password !== $passwordRepeat)
            {
                Errors::throw(Errors::PASSWORDS_DO_NOT_MATCH);
            }

            $user->setPassword($this->encoder->encodePassword($user, $password));
        }
        $user->setAvatar($avatar);

        return $user;
    }
}