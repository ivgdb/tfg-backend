<?php


namespace App\UseCases\Videogame;


use App\Repository\VideogameRepository;

class SearchVideogamesUseCase
{
    private $videogameRepository;

    public function __construct(VideogameRepository $videogameRepository)
    {
        $this->videogameRepository = $videogameRepository;
    }

    public function execute(?string $query = null )
    {
        if($query){
            if($query === 'latest'){
                return $this->videogameRepository->orderByDate();
            }
            return $this->videogameRepository->findWithNameLike($query);
        }

        return $this->videogameRepository->orderByTitle();
    }
}