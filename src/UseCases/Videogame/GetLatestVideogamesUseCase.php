<?php


namespace App\UseCases\Videogame;

use App\Repository\VideogameRepository;

class GetLatestVideogamesUseCase
{
    private $videogameRepository;

    public function __construct(VideogameRepository $videogameRepository)
    {
        $this->videogameRepository = $videogameRepository;
    }

    public function execute()
    {
        return $this->videogameRepository->orderByDate();
    }
}