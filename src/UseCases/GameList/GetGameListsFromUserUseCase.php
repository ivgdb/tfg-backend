<?php


namespace App\UseCases\GameList;


use App\Entity\User;
use App\Repository\GameListRepository;

class GetGameListsFromUserUseCase
{
    private $gameListRepository;

    public function __construct(GameListRepository $gameListRepository)
    {
        $this->gameListRepository = $gameListRepository;
    }

    public function execute(User $user)
    {
       return $user->getGameLists();
    }
}