<?php


namespace App\UseCases\GameList;


use App\Entity\GameList;
use App\Entity\User;
use App\Entity\Videogame;
use App\Enum\Errors;
use App\Repository\GameListRepository;
use App\Repository\VideogameRepository;

class AddGameToGameListUseCase
{
    private $gameListRepository;

    public function __construct(GameListRepository $gameListRepository)
    {
        $this->gameListRepository = $gameListRepository;
    }

    public function execute(User $user, GameList $gameList, Videogame $videogame)
    {
        if( $gameList->getUser() !== $user )
        {
            Errors::throw(Errors::FORBIDDEN);
        }

        $videogameAlreadyInGameList = $this->gameListRepository->findWithGame($videogame, $gameList);
        if($videogameAlreadyInGameList){
            Errors::throw(Errors::GAME_ALREADY_ADDED);
        }

        $gameList->addGame($videogame);

        return $gameList;
    }
}