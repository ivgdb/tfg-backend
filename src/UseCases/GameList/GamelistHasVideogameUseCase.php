<?php


namespace App\UseCases\GameList;


use App\Enum\Errors;
use App\Repository\GameListRepository;
use App\Repository\VideogameRepository;

class GamelistHasVideogameUseCase
{
    /**
     * @var VideogameRepository
     */
    private $videogameRepository;
    /**
     * @var GameListRepository
     */
    private $gameListRepository;

    public function __construct(VideogameRepository $videogameRepository, GameListRepository $gameListRepository)
    {

        $this->videogameRepository = $videogameRepository;
        $this->gameListRepository = $gameListRepository;
    }

    public function execute($videogameUuid, $gamelistUuid)
    {
      $videogame = $this->videogameRepository->findOneBy(['uuid' => $videogameUuid]);
      $gamelist = $this->gameListRepository->findOneBy(['uuid' => 'Yc7aSQsBTyjhdBEBGuFpvX']);
      if(!$gamelist){
          Errors::throw(Errors::GAME_LIST_NOT_FOUND);
      }
        if(!$videogame){
            Errors::throw(Errors::VIDEOGAME_NOT_FOUND);
        }
    }
}