<?php


namespace App\UseCases\GameList;


use App\Entity\GameList;
use App\Entity\User;
use App\Entity\Videogame;
use App\Enum\Errors;
use App\Repository\GameListRepository;
use App\Repository\VideogameRepository;

class CreateGameListUseCase
{
    private $gameListRepository;
    /**
     * @var VideogameRepository
     */
    private $videogameRepository;

    public function __construct(GameListRepository $gameListRepository, VideogameRepository $videogameRepository)
    {
        $this->gameListRepository = $gameListRepository;
        $this->videogameRepository = $videogameRepository;
    }

    public function execute(User $user, string $name, $videogameUuid)
    {
        $gameListExists = $this->gameListRepository->findOneBy(['user' => $user, 'name' => $name]);

        if($gameListExists)
        {
            Errors::throw(Errors::GAME_LIST_ALREADY_EXISTS);
        }
        $videogame = $this->videogameRepository->findOneBy(['uuid' => $videogameUuid]);
        if(!$videogame) {
            Errors::throw(Errors::VIDEOGAME_NOT_FOUND);
        }
        $gameList = new GameList($user, $name);
        $gameList->addGame($videogame);

        $this->gameListRepository->persist($gameList);

        return $gameList;
    }
}