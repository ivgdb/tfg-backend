<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use PascalDeVink\ShortUuid\ShortUuid;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VideogameRepository")
 */
class Videogame
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Exclude()
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="json",nullable=true)
     */
    private $genres = [];

    /**
     * @ORM\Column(type="json",nullable=true)
     */
    private $developers = [];

    /**
     * @ORM\Column(type="json",nullable=true)
     */
    private $publishers = [];

    /**
     * @ORM\Column(type="json",nullable=true)
     */
    private $platforms = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $franchise;

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $releaseDate;

    /**
     * @ORM\Column(type="string", length=5000)
     */
    private $summary;

    /**
     * @ORM\Column(type="json",nullable=true)
     */
    private $artworks = [];

    /**
     * @ORM\Column(type="json",nullable=true)
     */
    private $videos = [];

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cover;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $rating;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rating_count;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $screenshots = [];

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    private $storyline;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ratings", mappedBy="videogame",cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $ratings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="videogame", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $metacritic;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $websites;

    public function __construct($title, $summary, $releaseDate)
    {
        $this->setUuid((new ShortUuid())->encode(Uuid::uuid1()));

        $this->setTitle($title);
        $this->setSummary($summary);
        $this->setReleaseDate($releaseDate);
        $this->ratings = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getGenres(): ?array
    {
        return $this->genres;
    }

    public function setGenres(array $genres): self
    {
        $this->genres = $genres;

        return $this;
    }

    public function getDevelopers(): ?array
    {
        return $this->developers;
    }

    public function setDevelopers(array $developers): self
    {
        $this->developers = $developers;

        return $this;
    }

    public function getPublishers(): ?array
    {
        return $this->publishers;
    }

    public function setPublishers(array $publishers): self
    {
        $this->publishers = $publishers;

        return $this;
    }

    public function getPlatforms(): ?array
    {
        return $this->platforms;
    }

    public function setPlatforms(array $platforms): self
    {
        $this->platforms = $platforms;

        return $this;
    }

    public function getFranchise(): ?string
    {
        return $this->franchise;
    }

    public function setFranchise(string $franchise): self
    {
        $this->franchise = $franchise;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    public function getArtworks(): ?array
    {
        return $this->artworks;
    }

    public function setArtworks(array $artworks): self
    {
        $this->artworks = $artworks;

        return $this;
    }

    public function getVideos(): ?array
    {
        return $this->videos;
    }

    public function setVideos(array $videos): self
    {
        $this->videos = $videos;

        return $this;
    }


    public function getUuid()
    {
        return $this->uuid;
    }


    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(?string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getRating(): ?float
    {
        return $this->rating;
    }

    public function setRating(?float $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getRatingCount(): ?int
    {
        return $this->rating_count;
    }

    public function setRatingCount(?int $rating_count): self
    {
        $this->rating_count = $rating_count;

        return $this;
    }

    public function getScreenshots(): ?array
    {
        return $this->screenshots;
    }

    public function setScreenshots(?array $screenshots): self
    {
        $this->screenshots = $screenshots;

        return $this;
    }

    public function getStoryline(): ?string
    {
        return $this->storyline;
    }

    public function setStoryline(?string $storyline): self
    {
        $this->storyline = $storyline;

        return $this;
    }

    /**
     * @return Collection|Ratings[]
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRatings(Ratings $ratings): self
    {
        if (!$this->ratings->contains($ratings)) {
            $this->ratings[] = $ratings;
            $ratings->setVideogame($this);
            //TODO: FIX - RATING IS THE AVERAGE - CALCULATE NEW AVERAGE
            $this->setRatingCount($this->getRatingCount()+1);
            $updatedRating = ($this->getRating()+$ratings)/$this->getRatingCount();
            $this->setRating($updatedRating);
        }

        return $this;
    }

    public function removeRatings(Ratings $ratings): self
    {
        if ($this->ratings->contains($ratings)) {
            $this->ratings->removeElement($ratings);
            //TODO: FIX - RATING IS THE AVERAGE - CALCULATE NEW AVERAGE
            $this->setRatingCount($this->getRatingCount()-1);
            $updatedRating = ($this->getRating()-$ratings)/$this->getRatingCount();
            $this->setRating($updatedRating);
            // set the owning side to null (unless already changed)
            if ($ratings->getVideogame() === $this) {
                $ratings->setVideogame(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setVideogame($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getVideogame() === $this) {
                $comment->setVideogame(null);
            }
        }

        return $this;
    }

    public function getMetacritic(): ?float
    {
        return $this->metacritic;
    }

    public function setMetacritic(?float $metacritic): self
    {
        $this->metacritic = $metacritic;

        return $this;
    }

    public function getWebsites(): ?array
    {
        return $this->websites;
    }

    public function setWebsites(?array $websites): self
    {
        $this->websites = $websites;

        return $this;
    }
}
