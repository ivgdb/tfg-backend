<?php

namespace App\Entity;

use App\Enum\Errors;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use PascalDeVink\ShortUuid\ShortUuid;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GameListRepository")
 */
class GameList
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Videogame")
     */
    private $videogames;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="gameLists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    public function __construct($user, $name)
    {
        $this->setUuid((new ShortUuid())->encode(Uuid::uuid1()));
        $this->videogames = new ArrayCollection();
        $this->setUser($user);
        $this->setName($name);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Videogame[]
     */
    public function getVideogames(): Collection
    {
        return $this->videogames;
    }

    public function addGame(Videogame $game): self
    {
        if ($this->videogames->contains($game)) {
            Errors::throw(Errors::VIDEOGAME_ALREADY_EXISTS);
        }
        $this->videogames[] = $game;
        return $this;
    }

    public function removeGame(Videogame $game): self
    {
        if ($this->videogames->contains($game)) {
            $this->videogames->removeElement($game);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
