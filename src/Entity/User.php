<?php

namespace App\Entity;

use App\Enum\Errors;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use PascalDeVink\ShortUuid\ShortUuid;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @Vich\Uploadable
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Serializer\Exclude()
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $uuid;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=180)
     */
    private $name;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string")
     * @Serializer\Exclude()
     */
    private $password;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $avatar;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\GameList", mappedBy="user",cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $gameLists;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $nickname;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Ratings", mappedBy="user",cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $ratings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Comment", mappedBy="user",cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $comments;

    public function __construct($email,$nickname, $name)
    {
        $this->setUuid((new ShortUuid())->encode(Uuid::uuid1()));

        $this->setUsername($email);
        $this->setNickname($nickname);
        $this->setName($name);

        $this->gameLists = new ArrayCollection();
        $this->ratings = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function setPassword($password): void
    {
        if(!$password){
            Errors::throw(Errors::USER_SHOULD_HAVE_PASSWORD);
        }
        $this->password = $password;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUuid()
    {
        return $this->uuid;
    }

    public function setUuid($uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles): void
    {
        $this->roles = $roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function setUsername($email): void
    {
        if(!$email) {
            Errors::throw(Errors::USER_SHOULD_HAVE_EMAIL);
        }
        $this->email = strtolower(trim($email));
    }

    public function getName()
    {

        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = trim($name);
    }

    public function eraseCredentials()
    {
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return Collection|GameList[]
     */
    public function getGameLists(): Collection
    {
        return $this->gameLists;
    }

    public function addGameList(GameList $gameList): self
    {
        if (!$this->gameLists->contains($gameList)) {
            $this->gameLists[] = $gameList;
            $gameList->setUser($this);
        }

        return $this;
    }

    public function removeGameList(GameList $gameList): self
    {
        if ($this->gameLists->contains($gameList)) {
            $this->gameLists->removeElement($gameList);
            // set the owning side to null (unless already changed)
            if ($gameList->getUser() === $this) {
                $gameList->setUser(null);
            }
        }

        return $this;
    }

    public function getCustomLists(): ?string
    {
        return $this->customLists;
    }

    public function setCustomLists(string $customLists): self
    {
        $this->customLists = $customLists;

        return $this;
    }

    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    public function setNickname($nickname): self
    {
        $this->nickname = trim($nickname);

        return $this;
    }

    /**
     * @return Collection|Ratings[]
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRating(Ratings $rating): self
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings[] = $rating;
            $rating->setUser($this);
        }

        return $this;
    }

    public function removeRating(Ratings $rating): self
    {
        if ($this->ratings->contains($rating)) {
            $this->ratings->removeElement($rating);
            // set the owning side to null (unless already changed)
            if ($rating->getUser() === $this) {
                $rating->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setUser($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getUser() === $this) {
                $comment->setUser(null);
            }
        }

        return $this;
    }



}
