<?php

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class SerializationListener
{
    protected $serializer;
    protected $em;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $em)
    {
        $this->serializer = $serializer;
        $this->em = $em;
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        $value = $event->getControllerResult();
        $this->em->flush();

        $context = SerializationContext::create();
        $context->enableMaxDepthChecks();
        $response = new Response($this->serializer->serialize($value, "json", $context));
        $event->setResponse($response);
    }
}
