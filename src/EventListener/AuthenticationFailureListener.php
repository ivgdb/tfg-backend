<?php

namespace App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationFailureEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @param AuthenticationFailureEvent $event
 */
class AuthenticationFailureListener
{
    public function onAuthenticationFailureResponse(AuthenticationFailureEvent $event)
    {
        throw new HttpException(401, 'BAD_CREDENTIALS');
    }
}
