<?php

namespace App\DataFixtures;


use App\Entity\GameList;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use PascalDeVink\ShortUuid\ShortUuid;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    public static $user = [
        'uuid' => '1234',
        'email' => 'robot@q.com',
        'password' => '1234',
        'name' => 'Elliot Alderson',
        'nickname' => 'MrRobot',
        'phoneNumber' => '999999999'
    ];

    public $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User(
            static::$user['email'],
            static::$user['nickname'],
            static::$user['name']

        );
        $user->setPassword($this->encoder->encodePassword($user, '1234'));
        $user->setUuid(static::$user['uuid']);
        $favoriteGamesList = new GameList($user, 'Favorite Games');
        $manager->persist($favoriteGamesList);
        $wishList = new GameList($user, 'Wish List');
        $manager->persist($wishList);

        $user->addGameList($favoriteGamesList);
        $user->addGameList($wishList);

        $manager->persist($user);

        $manager->flush();
    }
}
