<?php


namespace App\Controller;


use App\Entity\Videogame;
use App\Repository\RatingsRepository;
use App\Repository\VideogameRepository;
use App\UseCases\Rating\AddRatingToGameUseCase;
use App\UseCases\Rating\GetByUserAndVideogameUseCase;
use App\UseCases\Rating\GetRatingsByUserUseCase;
use App\UseCases\Rating\RemoveRatingFromGameUseCase;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Rest\Route("/api/ratings")
 */
class RatingController extends BaseController
{
    /**
     * @Post("/")
     */
    public function createRating(EntityManagerInterface $em, RatingsRepository $ratingsRepository, VideogameRepository $videogameRepository)
    {
        $rating = (new AddRatingToGameUseCase($ratingsRepository, $videogameRepository))->execute(
            $this->getUser(),
            $this->param('videogame'),
            $this->param('rating')
        );

        $em->persist($rating);

        return $rating;
    }

    /**
     * @Rest\Get("/")
     */
    public function getRatingsByUser(RatingsRepository $ratingsRepository, Request $request)
    {
        $getRatingsByUserUseCase = new GetRatingsByUserUseCase($ratingsRepository);
        $order = $request->query->get('order');
        $ratings = $getRatingsByUserUseCase->execute($this->getUser(), $order);

        return $ratings;
    }

    /**
     * @Rest\Get("/{videogame}")
     */
    public function getRatingByUserAndVideogame($videogame, RatingsRepository $ratingsRepository, VideogameRepository $videogameRepository)
    {
        $rating = (new GetByUserAndVideogameUseCase(
            $ratingsRepository,
            $videogameRepository
        ))->execute(
            $this->getUser(),
            $videogame
        );
        return $rating;

    }

    /**
     * @Rest\Delete("/game/{uuid}")
     */
    public function deleteRating(RatingsRepository $ratingsRepository, Videogame $videogame)
    {
        (new RemoveRatingFromGameUseCase($ratingsRepository))->execute($this->getUser(), $videogame);
    }
}