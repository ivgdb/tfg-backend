<?php

namespace App\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;


class BaseController extends AbstractFOSRestController
{
    public function repo($class)
    {
        return $this->getDoctrine()->getRepository($class);
    }

    protected function param($param)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();
        return $request->request->get($param);
    }
}