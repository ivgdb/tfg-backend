<?php


namespace App\Controller;

use App\Entity\GameList;
use App\Entity\User;
use App\Enum\Status;
use App\Repository\GameListRepository;
use App\Repository\VideogameRepository;
use App\UseCases\Database\UpdateDatabaseFromApiUseCase;
use App\Entity\Videogame;
use App\UseCases\GameList\AddGameToGameListUseCase;
use App\UseCases\GameList\CreateGameListUseCase;
use App\UseCases\GameList\GamelistHasVideogameUseCase;
use App\UseCases\GameList\GetGameListsFromUserUseCase;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Unirest;

/**
 * @Rest\Route("/api/gamelists")
 */
class GameListController extends BaseController
{
    /**
     * @Post("/")
     */
    public function createGameList(GameListRepository $gameListRepository, VideogameRepository $videogameRepository)
    {
        $gameList = (new CreateGameListUseCase($gameListRepository, $videogameRepository))->execute(
            $this->getUser(),
            $this->param('name'),
            $this->param('videogame')
        );

        return $gameList;
    }

    /**
     * @Post("/add")
     */
    public function addGameToGameList(GameListRepository $gameListRepository, VideogameRepository $videogameRepository)
    {
        $gameList = (new AddGameToGameListUseCase($gameListRepository))->execute(
            $this->getUser(),
            $gameListRepository->findOneBy(['uuid' => $this->param('gamelist')]),
            $videogameRepository->findOneBy(['uuid' => $this->param('videogame')])
        );

        return $gameList;
    }

    /**
     * @Rest\Get("/game")
     */
    public function playlistHasVideogame(GameListRepository $gameListRepository, VideogameRepository $videogameRepository){
        $videogame = $videogameRepository->findOneBy(['uuid' => $this->param('videogame')]);
        $playlist = $gameListRepository->findOneBy(['uuid' => $this->param('playlist'), 'user' => $this->getUser()]);
        if($playlist->getVideogames()->contains($videogame)){
            return true;
        }
        return false;
    }

    /**
     * @Rest\Get("/exists/videogame/{videogameUuid}")
     */
    public function getCheckVideogame(Videogame $videogameUuid, GameListRepository $gameListRepository) {
        $checkUseCase = new GamelistHasVideogameUseCase(
            $videogameUuid,
            $gameListRepository
        );

        $checkUseCase->execute(
            $this->param('videogame'),
            $this->param('playlist')
        );
    }

    /**
     * @Rest\Get("/")
     */
    public function getGameListsByUser(GameListRepository $gameListRepository)
    {
        return (new GetGameListsFromUserUseCase($gameListRepository))->execute($this->getUser());
    }

}
