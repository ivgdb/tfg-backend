<?php

namespace App\Controller;

use App\Entity\User;
use App\Enum\Errors;
use App\Repository\UserRepository;
use App\UseCases\User\GetRegisteredUsersUseCase;
use App\UseCases\User\RegisterUserUseCase;
use App\UseCases\User\UpdateUserUseCase;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Rest\Route("/api/users")
 */
class UserController extends BaseController
{
    /**
     * @Post("/register")
     */
    public function createUser(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, UserRepository $userRepository)
    {
        $user = $this->repo(User::class)->findOneBy(['email' => $this->param('email')]);
        if ($user) {
            throw new \Exception(Errors::USER_ALREADY_EXISTS);
        }

        $user = (new RegisterUserUseCase($userRepository, $encoder))->execute(
            $this->param('email'),
            $this->param('password'),
            $this->param('passwordRepeat'),
            $this->param('nickname'),
            $this->param('name')
        );
        foreach ($user->getGameLists() as $gamelist)
        {
            $em->persist($gamelist);
        }

        $em->persist($user);

        return [];
    }

    /**
     * @Post("/update")
     */
    public function updateUser(UserPasswordEncoderInterface $encoder)
    {
        (new UpdateUserUseCase($encoder))->execute(
            $this->getUser(),
            $this->param('email'),
            $this->param('nickname'),
            $this->param('password'),
            $this->param('passwordRepeat'),
            $this->param('name'),
            $this->param('avatar')
        );

        return [];
    }

    /**
     * @Get("/current")
     */
    public function getCurrentUser()
    {
        return $this->getUser();
    }

    /**
     * @Rest\Get("/{uuid}")
     */
    public function getUserByUuid(User $user)
    {
        return $user;
    }

    /**
     * @Rest\Get("/")
     */
    public function getRegisteredUsers(UserRepository $userRepository)
    {
        return (new GetRegisteredUsersUseCase($userRepository))->execute();
    }

    /**
     * @Rest\Delete("/delete")
     */
    public function deleteRegisteredUser(EntityManagerInterface $em)
    {
        $user = $this->getUser();
        $em->remove($user);

        return [];
    }
}
