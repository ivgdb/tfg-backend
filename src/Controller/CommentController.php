<?php


namespace App\Controller;

use App\Entity\Videogame;
use App\Repository\CommentRepository;
use App\Repository\VideogameRepository;
use App\UseCases\Comment\CreateCommentUseCase;
use App\UseCases\Comment\GetCommentsByVideogameUseCase;
use App\UseCases\Comment\GetUserCommentsByVideogameUseCase;
use App\UseCases\Comment\GetCommentsByUserUseCase;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;

/**
 * @Rest\Route("/api/comments")
 */
class CommentController extends BaseController
{

    /**
     * @Post("/")
     */
    public function createComment(VideogameRepository $videogameRepository)
    {
        $comment = (new CreateCommentUseCase($videogameRepository))->execute(
            $this->getUser(),
            $this->param('videogame'),
            $this->param('comment')
        );

        return $comment;
    }

    /**
     * @Rest\Get("/user/videogame")
     */
    public function getUserCommentsByVideogame(VideogameRepository $videogameRepository, CommentRepository $commentRepository)
    {
        $getUserCommentsByVideogameUseCase = new GetUserCommentsByVideogameUseCase(
            $videogameRepository,
            $commentRepository
        );
        $comments = $getUserCommentsByVideogameUseCase->execute(
            $this->getUser(),
            $this->param('videogame')
        );

        return $comments;
    }

    /**
     * @Rest\Get("/{uuid}")
     */
    public function getCommentsByVideogame(Videogame $videogame,VideogameRepository $videogameRepository, CommentRepository $commentRepository)
    {
        $getCommentsByVideogameUseCase = new GetCommentsByVideogameUseCase(
            $videogameRepository,
            $commentRepository
        );

        $comments = $getCommentsByVideogameUseCase->execute(
            $videogame
        );

        return $comments;
    }

    /**
     * @Rest\Get("/")
     */
    public function getCommentsByUser(CommentRepository $commentRepository)
    {
        $getCommentsByUserUseCase = new GetCommentsByUserUseCase(
            $commentRepository
        );

        $comments = $getCommentsByUserUseCase->execute($this->getUser());
        return $comments;
    }
}