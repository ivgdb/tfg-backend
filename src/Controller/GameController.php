<?php


namespace App\Controller;

use App\Entity\User;
use App\Enum\Status;
use App\Repository\VideogameRepository;
use App\UseCases\Database\UpdateDatabaseFromApiUseCase;
use App\Entity\Videogame;
use App\UseCases\Videogame\GetLatestVideogamesUseCase;
use App\UseCases\Videogame\SearchVideogamesUseCase;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Request;
use Unirest;

/**
 * @Rest\Route("/api/games")
 */
class GameController extends BaseController
{

    /**
     * @Post("/updateDB")
     */
    public function updateDBfromGiantBomb(EntityManagerInterface $em, VideogameRepository $videogameRepository)
    {
        $testQueries = ['fortnite'];
        $queries = ['batman','assassin','spider-man','god of war','rocket league','fortnite','last of us','call of duty','battlefield', 'star wars','red dead redemption','horizon zero dawn','grand theft auto', 'nba', 'witcher', 'uncharted', 'far cry', 'fallout', 'resident evil', 'fifa'];
        foreach ($queries as $query) {
            $videogames = (new UpdateDatabaseFromApiUseCase($videogameRepository))->execute('https://api-v3.igdb.com/games', $query);
            if ($videogames) {

                foreach ($videogames as $game) {
                    $em->persist($game);
                }
                $em->flush();
            }
        }

        return ['success' => true];

    }

    /**
     * @Rest\Get("/{uuid}")
     */
    public function getVideogameByUuid(Videogame $videogame)
    {
        return $videogame;
    }

    /**
     * @Rest\Get("/latest")
     */
    public function getLatestVideogames(VideogameRepository $videogameRepository)
    {
        return (new GetLatestVideogamesUseCase($videogameRepository))->execute();
    }

    /**
     * @Rest\Get("/")
     */
    public function searchVideogame(VideogameRepository $videogameRepository, Request $request)
    {
        $query = $request->query->get('query');
        return (new SearchVideogamesUseCase($videogameRepository))->execute($query);
    }

}
